# README #

This README documents the basic installation and deployment steps, and access
to the deployed Heroku instance.

## Not really a Licence ##

This repository contains the code corresponding to the BMAT developer test
(aka. RECRUITMENT PROCESS). All code herein is (c) by Jacek Wojdeł
(aka. CANDIDATE), no parts of it may be used to any other end than the
RECRUITMENT PROCESS evaluation without the CANDIDATE's prior consent.

## Installation ##

The code is a fairly standard Django (https://www.djangoproject.com/)
application with Django Rest Framework (http://www.django-rest-framework.org/).
It expects (and was tested on) Python version 3 language. All additional
requirements are listed in requirements.txt file, so using standard Python3
environment, running it locally should be as easy as:

    $ pip install -r requirements.txt
    $ python ./manage.py syncdb
    ...
    $ python ./manage.py test   # optional
    $ python ./manage.py runserver

### Unit tests ###

The provided unit-tests run over all requested endpoint, with the exception of
the `/get_top`, which fails with the descriptive message:

    $ ./manage.py test
    Creating test database for alias 'default'...
    ...F................
    ======================================================================
    FAIL: test (bmat.tests.GetTopTest)
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "/home/jacek/workspace/bmat/bmat/tests.py", line 324, in test
        """)
    AssertionError:
    Intentional failure with respect to testing the ranking functionality.
    The specification should be thoroughly revised first, and appropriate design
    decisions taken. See README for more information.


    ----------------------------------------------------------------------
    Ran 20 tests in 0.273s

    FAILED (failures=1)
    Destroying test database for alias 'default'...

The coverage summary for the tests is:

    Name                                      Stmts     Miss      Cover  Missing
    -----------------------------------------------------------------------------
    __init__.py                                   -        -         -
    settings.py                                  26        1      96.2%  99
    serializers.py                               76        -       100%
    models.py                                    25        2        92%  23, 33
    admin.py                                     14        -       100%
    tests.py                                    235        -       100%
    urls.py                                      11        -       100%
    views.py                                    120       31      74.2%  56, 205-216, 232-276
    wsgi.py                                       5        5         0%  10-16
    -----------------------------------------------------------------------------
    TOTAL                                       512       39      92.4%

### Heroku instance ###

A working copy of the software is deployed using Heroku infrastructure. It can
be found at the following address:

    http://bmat-devtest-jacekwojdel.herokuapp.com

The Django Rest Framework browsable APIs can be accessed (together with their
brief documentation) under following addresses:

    http://bmat-devtest-jacekwojdel.herokuapp.com/api/channels
    http://bmat-devtest-jacekwojdel.herokuapp.com/api/performers
    http://bmat-devtest-jacekwojdel.herokuapp.com/api/songs
    http://bmat-devtest-jacekwojdel.herokuapp.com/api/plays

All the endpoints specified in the test description are also available, and can
be tested with the provided test script (without `--add-data` on consecutive
runs):

    $ python test.py -H bmat-devtest-jacekwojdel.herokuapp.com -P 80 --add-data

Note that the app is deployed on a free Heroku service, which means it might
need time to wake-up at the first access.

## About the code ##

### Framework choice ###

Why use Python/Django? Mostly, because it is the framework that I am most
familiar with when it comes to the web-applications. I find it flexible and
extremely expressive (i.e. few lines of code lead to a lot of useful
functionality). While people sometimes complain about its deficiencies with
respect to the modern web-apps (like serving single-page app, with a lot of
static conrent [JS]... I agree, Djangular is a terrible mess and pain), it is
certainly very capable of delivering results when it comes to the REST-like
APIs.

Why DjangoRestFramework? Well, this is in my opinion the best solution for
standard RESTful data acces. Using DRF, one gets full CRUD interface by
basically just declaring the data models, all of it easily testable, with
"browsable-API" paradigm. Actualy, one could claim that for the presented
specification, DRF is a bit of an overkill. I could have also quickly define
the 7 endpoints as regular Django views, and the amount of code would be
comparable to the one presented. However, using DRF is a future-proof solution.
One should expect that  "chief of product" will walk in and require
"/update_play" endpoint in the near future. With DRF, it will be a matter of
simply adding a wrapper to the appropriate SomeModelViewSet.update() function,
so that the return dictionary is according to the specification. In the long
run, I could even hope to convince him to accept using HTTP response status
codes for communication of the request status (rather than 'code' attribute,
as it is now), therefore removing the need for wrappers at all (just straight
DRF responses).

Is it scaleable? Well, I can only say that if the body of data becomes huge,
the bottleneck will soon become the database, rather than the fact that I use
Python/Django. With that in mind, one should make sure that the calls use as
few trips to the DB as possible (see discussion on /get_top pethod) in the code
itself. And then, make sure that whatever DB backend there is, it is configured
properly, and given enough resources to operate on.

### What needs to be improved ###

At the moment, I think the most important thing to improve (redesign according
to improved specification) is the /get_top endpoint. There is a paragraph for
that.

But, most importantly, before gouing for production, an authorization layer
must be introduced. At the moment, there is absolutely no control over access
to the endpoints (it was not mentioned in the test specification); anyone with
an URL can CRUD any data. Obviously, this cannot be like that in the production
environment. While limiting access to the endpoint is a single line of code in
DRF, one needs to invest time to actuall specify what access restrictions will
be needed. Should they be:

* user and/or usergroup based - keeping a user store on site, limiting access
  accordingly
* application based - keeping a list of applications allowed to do operations,
  negotiating rights at connection
* token based with external authority - for example OAuth(2) based scheme for
  restricting access
* many more...

Before figuring this out... the code is a sure disaster anywhere near the
production.

### Will it scale? ###
#### ... or: what's wrong with /get_top endpoint ####

Let us start with some comments about deficiencies in specification. The
specification is somewhat inconsistent, at one place it says:

> For each song, provide their performer, playcount, the number of weeks they’ve
> been in the top 40, the position they had the previous week.

but then, in endpoint specification, and in the test-script it looks only at
`performer`, `title`, `plays`, `previous_plays`, `rank`, and `previous_rank`
fields.

In the specification it suggests both `start` and `end` query parameters,
without telling us what would be the meaning of the query if they are *not* in
exactly 1-week distance. The test script, on the other hand just uses `start`
parameter.

The above issues would quickly be ironed out during a first daily stand-up in
a regular development process. In the case of a test, I decided to take the
following decisions:

* The returned data will only contain the ranking of a week starting with the
  `start` datetime, and contain `previous_*` attributes for the preceeding week
  only.
* The previous ranking numbers refer always to the ranking with the specified
  "start-of-week" datetime, and the `limit` maximal number of positions on the
  list.

This decision leads to the following design choices:

* The ranking is requeried from the DB each time the endpoint is accessed,
  because there is no simple way to effectively cache the results if the week
  starting point can be specified at an arbitrary point of time.

This resulted in the code that tries to limit the Django-related CPU load by
constructing a query that goes beyond typical Django capabilities (note that I
was tempted to use django-aggrgate-if, but decided to be explicit this time).
It is possibly, the least tested part of the code, that's why `./manage.py test`
fails with a descriptive message on this part of code.

Of course, the real world is probably different. One would imagine that:

* the week always starts on Monday 00:00 local time w.r.t. the channel (or any
  other predefined time), and
* no historical data is added to the DB (ir it is added infrequently).

In such a case, per-channel totals can be stored on-the-fly at each `/add_play`
invocation. The results can be stored in an appropriate table and simply pulled
from the DB when needed. The `Channel` would see the extension to contain TZ
and `startofweek` data, the `/get_top` would just accept week number, and the
thing would scale way better than it does now.

## Workload summary ##

* About 20 working hours in 4 working days (mostly nights).

## Who do I talk to? ##

* Jacek Wojdeł <j.c.wojdeł@gmail.com>