import json
from django.test import TestCase, Client

from .models import Channel, Performer, Song, Play

ADD_CHANNEL_URI = '/add_channel'
ADD_PERFORMER_URI = '/add_performer'
ADD_SONG_URI = '/add_song'
ADD_PLAY_URI = '/add_play'
GET_SONG_PLAYS_URI = '/get_song_plays'
GET_CHANNEL_PLAYS_URI = '/get_channel_plays'


class ChannelTest(TestCase):
    initial_channel_data = {'name': 'channel1'}
    new_channel_data = {'name': 'channel2'}

    def setUp(self):
        Channel.objects.create(**self.initial_channel_data)

    def test_add_channel(self):
        # Make sure there is no spurious data
        self.assertRaises(Channel.DoesNotExist,
                          Channel.objects.get, **self.new_channel_data)

        c = Client()
        response = c.post(ADD_CHANNEL_URI, self.new_channel_data)
        self.assertEqual(response.status_code, 201)
        data = json.loads(response.content.decode())
        self.assertEqual(data['code'], 0)
        self.assertEqual(data['result']['name'], self.new_channel_data['name'])

        obj = Channel.objects.get(**self.new_channel_data)
        self.assertEqual(obj.name, self.new_channel_data['name'])

    def test_add_existing_channel(self):
        c = Client()
        response = c.post(ADD_CHANNEL_URI, self.initial_channel_data)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        self.assertEqual(data['code'], 0)
        self.assertEqual(Channel.objects.count(), 1)

    def test_add_invalid_channel(self):
        c = Client()
        for bad_data in [{}, {'name': ''}, {'badfield': 'value'}]:
            response = c.post(ADD_CHANNEL_URI, bad_data)
            self.assertEqual(response.status_code, 400)
            data = json.loads(response.content.decode())
            self.assertNotEqual(data['code'], 0)
            self.assertEqual(Channel.objects.count(), 1)


class PerformerTest(TestCase):
    initial_performer_data = {'name': 'performer1'}
    new_performer_data = {'name': 'performer2'}

    def setUp(self):
        Performer.objects.create(**self.initial_performer_data)

    def test_add_performer(self):
        # Make sure there is no spurious data
        self.assertRaises(Performer.DoesNotExist,
                          Performer.objects.get, **self.new_performer_data)

        c = Client()
        response = c.post(ADD_PERFORMER_URI, self.new_performer_data)
        self.assertEqual(response.status_code, 201)
        data = json.loads(response.content.decode())
        self.assertEqual(data['code'], 0)
        self.assertEqual(data['result']['name'],
                         self.new_performer_data['name'])

        obj = Performer.objects.get(**self.new_performer_data)
        self.assertEqual(obj.name, self.new_performer_data['name'])

    def test_add_existing_performer(self):
        c = Client()
        response = c.post(ADD_PERFORMER_URI, self.initial_performer_data)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        self.assertEqual(data['code'], 0)
        self.assertEqual(Performer.objects.count(), 1)

    def test_add_invalid_performer(self):
        c = Client()
        for bad_data in [{}, {'name': ''}, {'badfield': 'value'}]:
            response = c.post(ADD_PERFORMER_URI, bad_data)
            self.assertEqual(response.status_code, 400)
            data = json.loads(response.content.decode())
            self.assertNotEqual(data['code'], 0)
            self.assertEqual(Performer.objects.count(), 1)


class SongTest(TestCase):
    initial_song_data = {'title': 'song1', 'performer': 'performer1'}
    new_song_data = {'title': 'song2', 'performer': 'performer2'}

    def setUp(self):
        p = Performer.objects.create(name=self.initial_song_data['performer'])
        Song.objects.create(title=self.initial_song_data['title'],
                            performer=p)

    def test_add_song(self):
        c = Client()
        response = c.post(ADD_SONG_URI, self.new_song_data)
        self.assertEqual(response.status_code, 201)
        data = json.loads(response.content.decode())
        self.assertEqual(data['code'], 0)
        self.assertEqual(data['result']['title'], self.new_song_data['title'])
        self.assertEqual(data['result']['performer'],
                         self.new_song_data['performer'])
        obj = Song.objects.get(title=self.new_song_data['title'])
        self.assertEqual(obj.title, self.new_song_data['title'])
        self.assertEqual(obj.performer.name, self.new_song_data['performer'])
        self.assertEqual(Song.objects.count(), 2)
        self.assertEqual(Performer.objects.count(), 2)

        self.new_song_data['title'] = 'anothertitle'
        response = c.post(ADD_SONG_URI, self.new_song_data)
        self.assertEqual(data['code'], 0)

        self.assertEqual(Song.objects.count(), 3)
        self.assertEqual(Performer.objects.count(), 2)

    def test_add_existing_song(self):
        c = Client()
        response = c.post(ADD_SONG_URI, self.initial_song_data)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        self.assertEqual(data['code'], 0)
        self.assertEqual(Song.objects.count(), 1)
        self.assertEqual(Performer.objects.count(), 1)

    def test_add_invalid_song(self):
        c = Client()
        for bad_data in [{}, {'title': '', 'performer': ''}, {'badfield': 'value'}]:
            response = c.post(ADD_SONG_URI, bad_data)
            self.assertEqual(response.status_code, 400)
            data = json.loads(response.content.decode())
            self.assertNotEqual(data['code'], 0)
            self.assertEqual(Song.objects.count(), 1)


class PlayTest(TestCase):
    play_data = [{'title': 'song2', 'performer': 'performer2',
                  'channel': 'channel2', 'start': '2014-01-01T00:01:00Z',
                  'end': '2014-01-01T00:11:00Z'},
                 {'title': 'song2', 'performer': 'performer2',
                  'channel': 'channel2',
                  'start': '2014-01-01T00:12:00Z',
                  'end': '2014-01-01T00:15:00Z'},
                 {'title': 'song2', 'performer': 'performer2',
                  'channel': 'channel2',
                  'start': '2014-01-01T00:00:00Z',
                  'end': '2014-01-01T00:00:59Z'},
                 {'title': 'notthissong', 'performer': 'performer2',
                  'channel': 'notthischannel', 'start': '2014-01-01T00:01:00Z',
                  'end': '2014-01-01T00:11:00Z'}
                 ]

    get_song_plays_request = {'title': 'song2', 'performer': 'performer2',
                              'start': '2014-01-01T00:01:00Z',
                              'end': '2014-01-01T00:11:00Z'}
    get_channel_plays_request = {'channel': 'channel2',
                                 'start': '2014-01-01T00:01:00Z',
                                 'end': '2014-01-01T00:11:00Z'}

    def setUp(self):
        pass

    def test_add_play(self):
        c = Client()
        play_data = self.play_data[0].copy()
        response = c.post(ADD_PLAY_URI, play_data)
        self.assertEqual(response.status_code, 201)
        data = json.loads(response.content.decode())

        self.assertEqual(data['code'], 0)
        for k in self.play_data[0]:
            self.assertEqual(data['result'][k], play_data[k])

        # Adding first "play" creates also song, performer and channel
        self.assertEqual(Play.objects.count(), 1)
        self.assertEqual(Song.objects.count(), 1)
        self.assertEqual(Performer.objects.count(), 1)
        self.assertEqual(Channel.objects.count(), 1)

        # We can add the same 'play', but the other entities will be taken from DB
        response = c.post(ADD_PLAY_URI, play_data)
        self.assertEqual(Play.objects.count(), 2)
        self.assertEqual(Song.objects.count(), 1)
        self.assertEqual(Performer.objects.count(), 1)
        self.assertEqual(Channel.objects.count(), 1)

        # The same with a different performer, and we create a new song as well
        play_data['performer'] = "anotherperformer"
        response = c.post(ADD_PLAY_URI, play_data)
        self.assertEqual(Play.objects.count(), 3)
        self.assertEqual(Song.objects.count(), 2)
        self.assertEqual(Performer.objects.count(), 2)
        self.assertEqual(Channel.objects.count(), 1)

    def test_add_empty_field_play(self):
        c = Client()
        play_data = self.play_data[0]
        for k in play_data:
            bad_data = play_data.copy()
            bad_data[k] = ""
            response = c.post(ADD_PLAY_URI, bad_data)
            self.assertEqual(response.status_code, 400)
            data = json.loads(response.content.decode())
            self.assertNotEqual(data['code'], 0)
            self.assertEqual(Song.objects.count(), 0)

    def test_bad_time_play(self):
        c = Client()
        play_data = self.play_data[0]
        for k in ['start', 'end']:
            bad_data = play_data.copy()
            bad_data[k] = "badtime"
            response = c.post(ADD_PLAY_URI, bad_data)
            self.assertEqual(response.status_code, 400)
            data = json.loads(response.content.decode())
            self.assertNotEqual(data['code'], 0)
            self.assertEqual(Song.objects.count(), 0)

    def test_end_not_after_start_play(self):
        c = Client()
        bad_data = self.play_data[0].copy()
        bad_data['end'] = bad_data['start']
        response = c.post(ADD_PLAY_URI, bad_data)
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.content.decode())
        self.assertNotEqual(data['code'], 0)
        self.assertEqual(Song.objects.count(), 0)

    def prepare_all_plays(self):
        c = Client()
        for play_data in self.play_data:
            c.post(ADD_PLAY_URI, play_data)

    def test_get_song_plays(self):
        self.prepare_all_plays()
        c = Client()

        response = c.get(GET_SONG_PLAYS_URI, self.get_song_plays_request)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())

        self.assertEqual(data['code'], 0)
        self.assertEqual(len(data['result']), 1)
        self.assertEqual(data['result'][0]['channel'],
                         self.play_data[0]['channel'])

    def test_get_song_plays_bad_args(self):
        self.prepare_all_plays()
        c = Client()

        bad_request = self.get_song_plays_request.copy()
        bad_request['badarg'] = 'something'
        response = c.get(GET_SONG_PLAYS_URI, bad_request)
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.content.decode())
        self.assertNotEqual(data['code'], 0)

    def test_get_song_plays_missing_args(self):
        self.prepare_all_plays()
        c = Client()

        for f in self.get_song_plays_request:
            bad_request = self.get_song_plays_request.copy()
            del bad_request[f]
            response = c.get(GET_SONG_PLAYS_URI, bad_request)
            self.assertEqual(response.status_code, 400)
            data = json.loads(response.content.decode())
            self.assertNotEqual(data['code'], 0)

    def test_get_channel_plays(self):
        self.prepare_all_plays()
        c = Client()

        response = c.get(GET_CHANNEL_PLAYS_URI, self.get_channel_plays_request)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())

        self.assertEqual(data['code'], 0)
        self.assertEqual(len(data['result']), 1)
        self.assertEqual(data['result'][0]['title'],
                         self.play_data[0]['title'])
        self.assertEqual(data['result'][0]['performer'],
                         self.play_data[0]['performer'])

    def test_get_channel_plays_bad_args(self):
        self.prepare_all_plays()
        c = Client()

        bad_request = self.get_channel_plays_request.copy()
        bad_request['badarg'] = 'something'
        response = c.get(GET_CHANNEL_PLAYS_URI, bad_request)
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.content.decode())
        self.assertNotEqual(data['code'], 0)

    def test_get_channel_plays_missing_args(self):
        self.prepare_all_plays()
        c = Client()

        for f in self.get_channel_plays_request:
            bad_request = self.get_channel_plays_request.copy()
            del bad_request[f]
            response = c.get(GET_CHANNEL_PLAYS_URI, bad_request)
            self.assertEqual(response.status_code, 400)
            data = json.loads(response.content.decode())
            self.assertNotEqual(data['code'], 0)


class GetTopTest(TestCase):
    def test(self):
        self.fail("""
Intentional failure with respect to testing the ranking functionality.
The specification should be thoroughly revised first, and appropriate design
decisions taken. See README for more information.
        """)