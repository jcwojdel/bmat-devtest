from django.contrib import admin
from .models import Channel, Performer, Song, Play


class ChannelAdmin(admin.ModelAdmin):
    pass


class PerformerAdmin(admin.ModelAdmin):
    pass


class SongAdmin(admin.ModelAdmin):
    pass


class PlayAdmin(admin.ModelAdmin):
    pass

admin.site.register(Channel, ChannelAdmin)
admin.site.register(Performer, PerformerAdmin)
admin.site.register(Song, SongAdmin)
admin.site.register(Play, PlayAdmin)
