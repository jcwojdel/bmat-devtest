import datetime
import json

import dateutil.parser
import django_filters
from rest_framework import viewsets, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from .models import Channel, Performer, Song, Play
from .serializers import (ChannelSerializer, PerformerSerializer,
                          SongSerializer, PlaySerializer,
                          SongPlaysRequestSerializer,
                          ChannelPlaysRequestSerializer,
                          TopRequestSerializer)


class BMatViewSet(viewsets.ModelViewSet):
    """
    Simple extension of the basic view set functionality, which allows for
    returning the data according to the specification
    """

    def create_wrapper(self, request):
        """
        Wraps the ModelViewSet.create call and maps the returned data so that
        HTTP201 response results in code=0, and created object data as a
        result.
        All other responses and validation errors end up propagated to the
        returned code and errors fields of the response.

        NOTE: Here we're wasting a bit of CPU time in creating a response
              (self.create) and then discarding it for a new one. This is
              potentially wasteful, but probably not by much. If needed,
              the code can be rewritten by partially copying the logic from
              ModelViewSet.create method.
        """
        try:
            rt = self.create(request)
            return self.wrap_response(rt)
        except ValidationError as e:
            return self.wrap_exception(e)

    def wrap_response(self, response):
        """
        General wrapper for the responses returned from the django rest
        framework. It returns JSON requested in the specification with the
        original data in 'result' and the 'code' set to 0 on success. On error
        it sets 'code' to the returned status code and places the data in
        'errors'.
        """
        if 200 <= response.status_code < 300:
            return_data = {'code': 0,
                           'result': response.data}
        else:
            return_data = {'code': response.status_code,
                           'errors': response.data}

        return Response(return_data, response.status_code)

    def wrap_exception(self, exc):
        """
        Wrapper for transforming exceptions into error responses according to
        specification. It sets the 'code' to 400, and places exception
        description in 'errors'.
        """
        return_data = {'code': status.HTTP_400_BAD_REQUEST,
                       'errors': exc.detail}

        return Response(return_data, status=status.HTTP_400_BAD_REQUEST)

    def list_with_strict_serializer(self, request, serializer_class):
        """
        Helper function which uses provided serializer class to validate the
        request parameters. It returns error response on validation errors or
        in case of invalid query parameters.
        If the request validates correctly, it returns a wrapped response from
        the 'list' endpoint of the viewset, so that it follows the JSON
        specification.
        """
        request_serializer = serializer_class(data=request.query_params)

        for f in request.query_params:
            if f not in request_serializer.fields:
                return self.wrap_exception(ValidationError(detail={"Invalid request parameter": f}))

        try:
            request_serializer.is_valid(raise_exception=True)
        except ValidationError as e:
            return self.wrap_exception(e)

        return self.wrap_response(self.list(request))


class ChannelViewSet(BMatViewSet):
    """
    /api/channels - CRUD API for Channel data.

    In addition to regular restful operation it provides also the following
    endpoint:

        POST /add_channel, data={name: ’channel_name’}

    It will add a new channel to the database, or do nothing if channel is already
    present. The returned JSON follows API specification.
    """
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer

    def add_channel(self, request):
        """
        /add_channel implementation
        """
        if self.queryset.filter(name=request.data.get('name')).exists():
            return Response({'code': 0, 'result': 'Ignoring existing channel'},
                            status=status.HTTP_200_OK)

        return self.create_wrapper(request)


class PerformerViewSet(BMatViewSet):
    """
    /api/performers - CRUD API for Performer data.

    In addition to regular restful operation it provides also the following
    endpoint:

        POST /add_performer, data={name: ’performer’}

    It will add a new performer to the database, or do nothing if performer is
    already present. The returned JSON follows API specification.
    """
    queryset = Performer.objects.all()
    serializer_class = PerformerSerializer

    def add_performer(self, request):
        """
        /add_performer implementation
        """
        if self.queryset.filter(name=request.data.get('name')).exists():
            return Response({'code': 0,
                             'result': 'Ignoring existing performer'},
                            status=status.HTTP_200_OK)

        return self.create_wrapper(request)


class SongViewSet(BMatViewSet):
    """
    /api/songs - CRUD API for Song data.

    In addition to regular restful operation it provides also the following
    endpoints:

    POST /add_song, data={title: ’song_name’, performer: ’performer_name’}

    It will add a new song to the database, or do nothing if a song is
    already present. The performer is created on the fly, if not present in the
    database. The returned JSON follows API specification.

    GET /get_top, data={channels: [’channel_name’, ...],
                        start: ’2014-10-21T00:00:00’, limit: 40}

    This queries the DB for the ranking corresponding to the week starting at
    'start' date, limiting the number of songs to 'limit', and querying for
    songs played on given 'channels'. It returns the following data (in
    partial accordance to the API specification; see README):

    {result: [{performer: ’performer’, title: ’title’, <- song data
               plays: 5, rank: 0, <- number of plays and rank (0-based)
               previous_plays: 7, previous_rank: 8 <- as above, for the previous week
               }, ... <- repeated lte 'limit' times],
     code: 0}

    If the song does not appear in previous ranking for the given 'limit', the
    returned previous_* attributes will be null.
    """
    queryset = Song.objects.all()
    serializer_class = SongSerializer

    def add_song(self, request):
        """
        /add_song implementation
        """
        if self.queryset.filter(title=request.data.get('title'),
                                performer__name=request.data.get('performer')).exists():
            return Response({'code': 0, 'result': 'Ignoring existing song'},
                            status=status.HTTP_200_OK)

        return self.create_wrapper(request)

    def query_ranking(self, query_data):
        """
        Helper function for retrieving ranking with playcounts for a week
        starting on a given day, for a list of channel ids etc.
        See SongViewSet.get_top() for more information.
        """
        # NOTE: bmat_play and bmat_song might be big, so we do not want to
        # loop and query multiple times. We use here QuerySet.extra() to make
        # the whole affair a single query to the DB.
        # For the rationale, and more info, look in README
        # NOTE: we assume that the play counts fot the given week if it
        # _starts_ in this week, so that we do not loose count of songs which
        # start in week=n, and end in week=n+1
        SQL_count_query = """
              SELECT COUNT(*)
              FROM bmat_play WHERE bmat_play.song_id = bmat_song.id
              AND bmat_play.start >= '%s'
              AND bmat_play.start < '%s'
              AND bmat_play.channel_id IN (%s)
              """ % (query_data['start'].isoformat(' '),
                     query_data['end'].isoformat(' '),
                     ','.join(query_data['channels']))

        songs = Song.objects.extra(select=dict(play_count=SQL_count_query))
        return songs.order_by('-play_count')[:query_data['limit']]

    def get_top(self, request):
        """
        Implementation of the /get_top endpoint.
        It uses TopRequestSerializer to validate the input query. It expects
        'start', 'channels', and 'limit' to be specified, and nothing else.
        - channels - must be a string which parses into a JSON array of strings.
        - start - must be a parse'able datetime, which is assumed the start of
                  the week (the week then ends 7 days later)
        - limit - integer which limits the ranking length

        The method then makes two calls to self.query_ranking: one for the
        requested week, one for the preceding week. For discussion on how this
        will become bottleneck, see README.
        """
        request_serializer = TopRequestSerializer(data=request.query_params)

        for f in request.query_params:
            if f not in request_serializer.fields:
                return self.wrap_exception(ValidationError(detail={"Invalid request parameter": f}))

        try:
            request_serializer.is_valid(raise_exception=True)
        except ValidationError as e:
            return self.wrap_exception(e)

        data = request_serializer.data

        data_channels = json.loads(data['channels'])
        data_channels = [str(c.pk) for c in Channel.objects.filter(name__in=data_channels)]
        data_start = dateutil.parser.parse(data['start'])
        data_start_prev = data_start - datetime.timedelta(weeks=1)
        data_end = data_start + datetime.timedelta(weeks=1)
        data_limit = data['limit']

        current_ranking = list(self.query_ranking(dict(channels=data_channels,
                                                       start=data_start,
                                                       end=data_end,
                                                       limit=data_limit)))

        previous_ranking = list(self.query_ranking(dict(channels=data_channels,
                                                        start=data_start_prev,
                                                        end=data_start,
                                                        limit=data_limit)))
        for s in current_ranking:
            try:
                pr = previous_ranking.index(s, )
                s.previous_rank = pr
                s.previous_plays = previous_ranking[pr].play_count
            except ValueError:
                s.previous_rank = None
                s.previous_plays = None

        result = [dict(title=s.title,
                       performer=s.performer.name,
                       plays=s.play_count,
                       rank=i,
                       previous_rank=s.previous_rank,
                       previous_plays=s.previous_plays) for i, s in enumerate(current_ranking)]
        return Response({'code': 0,
                         'result': result})


class PlayFilter(django_filters.FilterSet):
    """
    A filterset which allows for filtering Plays table based on any of the
    relevant fields.
    """
    # NOTE: I wish this would have worked... unfortunately, django-filters
    # does not parse dates correctly.
    # start = django_filters.DateTimeFilter(name='start', lookup_type='gte')
    # end = django_filters.DateTimeFilter(name='end', lookup_type='lte')
    title = django_filters.CharFilter(name='song__title')
    performer = django_filters.CharFilter(name='song__performer__name')
    channel = django_filters.CharFilter(name='channel__name')

    class Meta:
        model = Play
        fields = ['title', 'performer', 'channel', ]


class PlayViewSet(BMatViewSet):
    """
    /api/plays - CRUD API for Play data.

    The 'list' endpoint incorporates filtering based on the following fields:
    - title - only songs with matching title
    - performer - only songs with matching performer
    - channel - only songs played on matching channel
    - start - only songs which start after that time
    - end - only songs which end before that time
    Any number of the conditions might be specified in GET operation.
    Examples:
      GET /api/plays/?title=songtitle&channnel=mychannel
      GET /api/plays/?performer=artist&start=2015-01-06 16%3A30%3A17

    In addition to regular restful operation it provides also the following
    endpoints:

    POST /add_play, data={title: ’song_name’, performer: performer_name’,
                          start: ’2014-10-21T18:41:00’,
                          end: ’2014-10-21T18:44:00’,
                          channel: ’channel_name’}

    It will add a new play to the database. The song, performer, and channel
    are created on the fly, if not present in the database. The returned JSON
    follows API specification.

    GET /get_song_plays, data={title: ’song_name’, performer: 'performer_name’,
                               start: ’2014-10-21T00:00:00’,
                               end: ’2014-10-28T00:00:00’}

    Returns list of plays for the specific song in the given time interval.
    This endpoint follows strict rules about the query data: all fields are
    required, and any unknown field will raise error. The returned JSON
    follows API specification.

    GET /get_channel_plays, data={channel: ’channel_name’,
                                  start: ’2014-10-21T00:00:00’,
                                  end: ’2014-10-28T00:00:00’}

    Returns list of plays for the specific channel in the given time interval.
    This endpoint follows strict rules about the query data: all fields are
    required, and any unknown field will raise error. The returned JSON
    follows API specification.
    """
    queryset = Play.objects.all()
    serializer_class = PlaySerializer
    filter_class = PlayFilter

    # This non-standard attribute is used by the PlaySerializer to limit fields
    # which are returned. If None, all regular fields are included (see
    # PlaySerializer implementation and get_song_plays, get_channel_plays
    # methods.
    fields = None

    def get_queryset(self):
        queryset = Play.objects.all()

        end = self.request.query_params.get('end', None)
        start = self.request.query_params.get('start', None)

        if end:
            end = dateutil.parser.parse(end)
            queryset = queryset.filter(end__lte=end)

        if start:
            start = dateutil.parser.parse(start)
            queryset = queryset.filter(start__gte=start)

        return queryset

    def list(self, *args, **kwargs):
        return super(PlayViewSet, self).list(*args, **kwargs)

    def add_play(self, request):
        """
        /add_play implementation

        NOTE: In contrast to other /add_* endpoints, no checks are made for the
        uniqueness of the object.
        """
        return self.create_wrapper(request)

    def get_song_plays(self, request):
        """
        /get_song_plays implementation
        """
        self.fields = ['channel', 'start', 'end', ]
        return self.list_with_strict_serializer(request,
                                                SongPlaysRequestSerializer)

    def get_channel_plays(self, request):
        """
        /get_channel_plays implementation
        """
        self.fields = ['title', 'performer', 'start', 'end', ]
        return self.list_with_strict_serializer(request,
                                                ChannelPlaysRequestSerializer)
