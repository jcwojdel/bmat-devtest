import logging

from rest_framework import serializers

from .models import Channel, Performer, Song, Play


logger = logging.getLogger('django.request')


class ChannelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Channel
        fields = ('url', 'name',)


class PerformerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Performer
        fields = ('url', 'name',)


class SongSerializer(serializers.HyperlinkedModelSerializer):
    performer = serializers.CharField(required=True)

    class Meta:
        model = Song
        fields = ('url', 'title', 'performer', )

    def create(self, validated_data):
        """
        Overrifing the create method, so that the performer is created on the
        fly, if needed.
        """
        performer, created = Performer.objects.get_or_create(
                                             name=validated_data['performer'])
        if created:
            logger.info('Created new performer from Song creation')

        validated_data['performer'] = performer

        return Song.objects.create(**validated_data)


class StartBeforeEndMixin():
    """
    A serializer mixin that can be used in all serializers that require
    start < end in validation. Just call the validate_start_end() method
    within validate(), and it will raise ValidationError when needed.
    """

    def validate_start_end(self, data):
        if not data['start'] < data['end']:
            raise serializers.ValidationError("End of play must be after start")
        return data


class PlaySerializer(serializers.HyperlinkedModelSerializer,
                     StartBeforeEndMixin):
    title = serializers.CharField(required=True)
    performer = serializers.CharField(required=True)
    channel = serializers.CharField(required=True)

    class Meta:
        model = Play
        fields = ('url', 'title', 'performer', 'channel', 'start', 'end',)

    def __init__(self, *args, **kwargs):
        """
        Overriding __init__, so that the serialization fields can be
        overwritten dynamically based on context. The view which wishes to
        change the standard set of fields should declare a 'fields' attribute
        with the list of fields to be displayed.
        """
        super(PlaySerializer, self).__init__(*args, **kwargs)

        fields = getattr(kwargs['context']['view'], 'fields', None)
        if fields is not None:
            for f in self.fields.keys():
                if f not in fields:
                    self.fields.pop(f)

    def validate(self, data):
        return self.validate_start_end(data)

    def create(self, validated_data):
        """
        Overriding create method to make sure that song, performer, and channel
        are created on the fly if needed.
        """
        performer, created = Performer.objects.get_or_create(
                                           name=validated_data['performer'])
        if created:
            logger.info('Created new performer from Play creation')

        song, created = Song.objects.get_or_create(
                                           title=validated_data['title'],
                                           performer=performer)
        if created:
            logger.info('Created new song from Play creation')

        channel, created = Channel.objects.get_or_create(
                                         name=validated_data['channel'])
        if created:
            logger.info('Created new channel from Play creation')

        del validated_data['title']
        del validated_data['performer']
        validated_data['song'] = song
        validated_data['channel'] = channel

        return Play.objects.create(**validated_data)


class SongPlaysRequestSerializer(serializers.Serializer,
                                 StartBeforeEndMixin):
    """
    Serializer used to check for correctness of the /get_song_plays request
    """
    title = serializers.CharField(required=True)
    performer = serializers.CharField(required=True)
    start = serializers.DateTimeField(required=True)
    end = serializers.DateTimeField(required=True)

    def validate(self, data):
        return self.validate_start_end(data)


class ChannelPlaysRequestSerializer(serializers.Serializer,
                                    StartBeforeEndMixin):
    """
    Serializer used to check for correctness of the /get_channel_plays request
    """
    channel = serializers.CharField(required=True)
    start = serializers.DateTimeField(required=True)
    end = serializers.DateTimeField(required=True)

    def validate(self, data):
        return self.validate_start_end(data)


class TopRequestSerializer(serializers.Serializer):
    """
    Serializer used to check for correctness of the /get_top request
    """
    channels = serializers.CharField(required=True)
    start = serializers.DateTimeField(required=True)
    limit = serializers.IntegerField(min_value=1, required=True)
