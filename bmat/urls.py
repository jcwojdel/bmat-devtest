from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.routers import SimpleRouter
from .views import (ChannelViewSet, PerformerViewSet, SongViewSet, PlayViewSet)

# Full CRUD access to the database, as provided by django-rest-framework
# This may be removed if a strict adherence to the specification is needed
# (that is: if only the endpoints from the specification should be active)
router = SimpleRouter()
router.register(r'api/channels', ChannelViewSet)
router.register(r'api/performers', PerformerViewSet)
router.register(r'api/songs', SongViewSet)
router.register(r'api/plays', PlayViewSet)
urlpatterns = router.urls

urlpatterns.extend(patterns('',
    # Endpoints from the API specification
    url(r'^add_channel', ChannelViewSet.as_view({'post': 'add_channel'}),
        name="add-channel"),
    url(r'^add_performer', PerformerViewSet.as_view({'post': 'add_performer'}),
        name="add-performer"),
    url(r'^add_song', SongViewSet.as_view({'post': 'add_song'}),
        name="add-song"),
    url(r'^add_play', PlayViewSet.as_view({'post': 'add_play'})),

    url(r'^get_song_plays', PlayViewSet.as_view({'get': 'get_song_plays'}),
        name="get-song-plays"),
    url(r'^get_channel_plays',
        PlayViewSet.as_view({'get': 'get_channel_plays'}),
        name="get-channel-plays"),
    url(r'^get_top',
        SongViewSet.as_view({'get': 'get_top'}),
        name="get-top"),

    # Standard django administrative endpoint. Feel free to remove if required.
    url(r'^admin/', include(admin.site.urls)),
))
