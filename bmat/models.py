from django.db import models


class Channel(models.Model):
    name = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return '%s' % (self.name,)


class Performer(models.Model):
    name = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return '%s' % (self.name,)


class Song(models.Model):
    title = models.CharField(max_length=64)
    performer = models.ForeignKey(Performer)

    def __str__(self):
        return '%s - %s' % (self.title, self.performer.name, )


class Play(models.Model):
    song = models.ForeignKey(Song)
    channel = models.ForeignKey(Channel)
    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return '%s(%s) at %s, %s-%s' % (self.song.title,
                                        self.song.performer.name,
                                        self.channel.name,
                                        self.start, self.end, )

    def title(self):
        return self.song.title

    def performer(self):
        return self.song.performer
